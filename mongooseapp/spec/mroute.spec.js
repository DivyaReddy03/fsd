var Request = require("request");
describe("server",()=>{
    beforeAll(()=>{
        server=require("../mroute")


    });
    describe("get /", ()=>{
        var status;
        beforeAll((done)=>{
            Request.get("http://localhost:4002/projects/", (error, response, body)=>{
                status=response.statusCode;
                done();
            });
        });
            it("status 200", ()=>{
                expect(status).toBe(200);
            });
        });
        describe("post/projects/add/project", ()=>{
            var status;
            beforeAll((done)=>{            
            Request.post("http://localhost:4002/projects/add/project", (error, response, body)=>{
                status=response.statusCode;
                done(); 
            });               
            });
            it("status 201", ()=>{
                expect(status).toBe(201);
            });
        });
        describe("post/projects/add/employee/:pid", ()=>{
            var status;
            beforeAll((done)=>{            
            Request.post("http://localhost:4002/projects/add/employee/3", (error, response, body)=>{
                status=response.statusCode;
                done(); 
            });               
            });
            it("status 201", ()=>{
                expect(status).toBe(201);
            });
        });
        describe("post/projects/del/employee/:pid/:eid", ()=>{
            var status;
            beforeAll((done)=>{            
            Request.post("http://localhost:4002/projects/del/employee/3/1", (error, response, body)=>{
                status=response.statusCode;
                done(); 
            });               
            });
            it("status 201", ()=>{
                expect(status).toBe(201);
            });
        });
        describe("post/projects/delete/:pid", ()=>{
            var status;
            beforeAll((done)=>{            
            Request.delete("http://localhost:4002/projects/delete/3", (error, response, body)=>{
                status=response.statusCode;
                done(); 
            });               
            });
            it("status 200", ()=>{
                expect(status).toBe(200);
            });
        });
});