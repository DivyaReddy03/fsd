let employees=[
    {
        id:1,
        name:"Vedha",
        email:"vedha@gmail.com",
        salary:600000,
        pId:"FMS2019",
        designation:"Team Lead"
    },
    {
        id:2,
        name:"Bindu",
        email:"bin@gmail.com",
        salary:550000,
        pId:"DIYP2019",
        designation:"Team Lead"
    },
    {
        id:3,
        name:"Dev",
        email:"dev@gmail.com",
        salary:1000000,
        pId:"FMS2019",
        designation:"Manager"
    },
    {
        id:4,
        name:"Sam",
        email:"bin@gmail.com",
        salary:900000,
        pId:"DIYP2019",
        designation:"Manager"
    }
    
]
let projects=[
    {
        pId:"FMS2019",
        pName:"Fix MY'Ship",
        Domain:"Java Full Stack"
    },
    {
        pId:"DIYP2019",
        pName:"DIY Popup's",
        Domain:"Java Full Stack"
    }
]
module.exports={
    employees,
    projects
}